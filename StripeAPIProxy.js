const StripeUtils = require('./util/StripeUtils');
const DDBUtils = require('./util/DDBUtils');
const {USER_ATTRIBUTES} = require('./util/Constants');
const {TENANT_ID, TENANT_NAME} = USER_ATTRIBUTES;

exports.handler = async (event) => {

    console.log(event);
    const operation = event['pathParameters'] && event['pathParameters']['operation'];
    const oldPlanId = event['queryStringParameters'] && event['queryStringParameters']['oldPlanId'];
    const newPlanId = event['queryStringParameters'] && event['queryStringParameters']['newPlanId'];
    const userDetails = event['requestContext']['authorizer']['claims'];

    if (operation && userDetails) {
        const tenantId = userDetails[TENANT_ID];
        const tenantName = userDetails[TENANT_NAME];

        const results = await DDBUtils.getSubscriptionByTenantId(tenantId);

        if ((results && results.Items && results.Items.length > 0)) {
            const subID = results.Items[0].sub_id;
            switch (operation) {
                case 'getDetails': {
                    let r = await StripeUtils.getSubscriptionDetails(subID, tenantId);
                    return getSuccessResponse(refineSubscriptionDetails(r));
                }
                case 'cancelSubscription': {
                    let r = await StripeUtils.cancelSubscriptionAtPeriodEnd(subID, tenantId);
                    return getSuccessResponse(refineSubscriptionDetails(r));
                }
                case 'abortCancellation': {
                    let r = await StripeUtils.abortSubscriptionCancellation(subID, tenantId);
                    return getSuccessResponse(refineSubscriptionDetails(r));
                }
                case 'updateSubscription': {
                    const subscriptionDetails = await StripeUtils.getSubscriptionDetails(subID, tenantId);
                    const subscriptionItem = subscriptionDetails.items.data.find(item => item.plan.id = oldPlanId);
                    const subscriptionItemId = subscriptionItem.id;
                    let r = await StripeUtils.updateSubscription(subID, subscriptionItemId, oldPlanId, newPlanId);
                    return getSuccessResponse(refineSubscriptionDetails(r));
                }
                default: {
                    console.log('Unsupported operation', operation);
                    throw new Error('Unsupported operation');
                }
            }
        } else if (operation === "createSession") {
            const body = JSON.parse(event['body']);


            const {billingEmail, orgName, orgAddress, successUrl, cancelUrl, planId} = body;
            let { postalCode, ...formattedOrgAddress} = orgAddress;
            formattedOrgAddress = { ...formattedOrgAddress, postal_code: orgAddress.postalCode };
            const orgAddressString = JSON.stringify(formattedOrgAddress);

            console.log("Creating customer with email: %s, org name: %s, org address: %s", orgAddressString, orgName, orgAddress);
            const customer = await StripeUtils.createCustomer(billingEmail, orgName, formattedOrgAddress, tenantId, tenantName);
            const customerId = customer.id;
            console.log("Customer successfully created. customer id: %s, email: %s", customerId, billingEmail);
            const session = await StripeUtils.createSession(customerId, tenantId, tenantName, billingEmail, planId, successUrl, cancelUrl);
            console.log("Session object successfully created: session id: %s", session.id);
            return getSuccessResponse(refineSessionDetails(session));
        } else {
            console.log("Not a Stripe subscribed account, tenant id:", tenantId, tenantName);
            throw new Error('Unsupported operation.');
        }
        return getSuccessResponse({"message": "Successfully executed"});
    } else {
        console.log("Unsupported operation", operation, "or user details missing");
        return getFailureResponse('Unsupported operation');
    }
};

function refineSubscriptionDetails(sub) {
    return {
        "subscriptionID": sub.id,
        "created": sub.created,
        "period_start": sub.current_period_start,
        "period_end": sub.current_period_end,
        "plan": sub.plan.nickname,
        "amount": sub.plan.amount / 100,
        "currency": sub.plan.currency,
        "status": sub.status,
        "cancel_at_period_end": sub.cancel_at_period_end
    };
}

function refineSessionDetails(session) {
    return {
        "sessionID": session.id,
        "billingAddress": session.billing_address_collection,
        "cancelUrl": session.cancel_url,
        "clientReferenceId": session.client_reference_id,
        "customer": session.customer,
        "customerEmail": session.customer_email,
        "subscription": session.subscription,
        "success_url": session.success_url
    };
}

function getSuccessResponse(data) {
    return {
        "headers": {
            "Access-Control-Allow-Origin": "*",
            "Access-Control-Allow-Methods": "*"
        },
        "statusCode": 200,
        "body": JSON.stringify(data)
    };
};

function getFailureResponse(data) {
    return {
        "headers": {
            "Access-Control-Allow-Origin": "*",
            "Access-Control-Allow-Methods": "*"
        },
        "statusCode": 500,
        "body": JSON.stringify(data)
    };
};
