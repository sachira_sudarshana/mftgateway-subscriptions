const moment = require('moment');
const StripeUtils = require('./util/StripeUtils');
;

exports.handler = async (event) => {
    console.log(event);

    try {
        console.log("Adding packages to Stripe");
        await StripeUtils.addStripePackages();
        return {
            "headers": {
                "Access-Control-Allow-Origin": "*",
                "Access-Control-Allow-Methods": "*"
            },
            "statusCode": 200,
            "body": JSON.stringify({})
        };
    } catch (e) {
        console.error("Error occurred while adding stripe packages", e);
        return {
            "headers": {
                "Access-Control-Allow-Origin": "*",
                "Access-Control-Allow-Methods": "*"
            },
            "statusCode": 500,
            "body": JSON.stringify({})
        };
    }
};
