const moment = require('moment');
const SlackUtils = require('./util/SlackUtils');
const StripeUtils = require('./util/StripeUtils');
const DDBUtils = require('./util/DDBUtils');

const STRIPE_EVENT_TYPES = {
    CUSTOMER: {
        CREATED: 'customer.created',
    },
    CHARGE: {
        SUCCEEDED: 'charge.succeeded',
        FAILED: 'charge.failed'
    },
    SUBSCRIPTION: {
        CREATED: 'customer.subscription.created',
        UPDATED: 'customer.subscription.updated',
        DELETED: 'customer.subscription.deleted'
    },
    SESSION: {
        COMPLETED: 'checkout.session.completed'
    }
};

exports.handler = async (event) => {
    console.log(event);

    const stripeEvent = await StripeUtils.validateAndGetStripeEvent(event);
    if (stripeEvent) {
        console.log("Processing stripe event");
        console.log(stripeEvent);

        const eventType = StripeUtils.getEventType(stripeEvent);
        const eventData = StripeUtils.getEventData(stripeEvent);

        console.log("Event type:", eventType);

        switch (eventType) {
            case STRIPE_EVENT_TYPES.CUSTOMER.CREATED: {
                await handleCustomerCreatedEvent(eventData);
                break;
            }
            case STRIPE_EVENT_TYPES.CHARGE.SUCCEEDED: {
                await handleChargeSucceededEvent(eventData);
                break;
            }
            case STRIPE_EVENT_TYPES.CHARGE.FAILED: {
                await handleChargeFailedEvent(eventData);
                break;
            }
            case STRIPE_EVENT_TYPES.SESSION.COMPLETED: {
                await handleSessionCompletedEvent(eventData);
                break;
            }
            case STRIPE_EVENT_TYPES.SUBSCRIPTION.CREATED: {
                await handleSubscriptionCreatedEvent(eventData);
                break;
            }
            case STRIPE_EVENT_TYPES.SUBSCRIPTION.UPDATED: {
                await handleSubscriptionUpdatedEvent(eventData);
                break;
            }
            case STRIPE_EVENT_TYPES.SUBSCRIPTION.DELETED: {
                await handleSubscriptionCancelledEvent(eventData);
                break;
            }

            default: {
                console.log('Unsupported event type received', eventType);
                await SlackUtils.sendErrorNotification("Unsupported event type", `Received unsupported event type ${SlackUtils.getTickedText(eventType)}`);
            }
        }

        return getSuccessResponse();

    } else {
        console.log("Stripe event not processed");
        await SlackUtils.sendErrorNotification("Stripe event not processed", "Stripe event not processed, probably due to validation failure.");
        return getSuccessResponse();
    }
};

async function handleCustomerCreatedEvent(eventData) {
    const tenantId = eventData.metadata['tenant_id'];
    const orgName = eventData.name;
    const billingAddress = eventData.address;
    const billingEmail = eventData.email;
    console.log("Updating tenant with billing details, tenantId: %s, organization Name: %s, billing Address: %s, billing Email: %s", tenantId, orgName, billingAddress, billingEmail);
    const persisted = await DDBUtils.updateTenantBillingDetails(tenantId, orgName, billingAddress, billingEmail);
    console.log("Successfully updated tenant with billing details, tenantId: %s, organization Name: %s, billing Address: %s, billing Email: %s", tenantId, orgName, billingAddress, billingEmail);
    await SlackUtils.sendErrorNotification("Customer Created", getCustomerDetails(eventData, persisted));
}

async function handleChargeSucceededEvent(eventData) {
    await SlackUtils.sendSuccessNotification('Payment Successful', getChargeDetails(eventData), 'moneybag');
}

async function handleChargeFailedEvent(eventData) {
    await SlackUtils.sendErrorNotification("Payment Failed", getChargeDetails(eventData));
}

async function handleSessionCompletedEvent(eventData) {
    console.log("session completed event data: %s", JSON.stringify(eventData));
    const subscriptionID = eventData.subscription;
    const clientReference = eventData.client_reference_id;

    const clientDetails = clientReference.split('#');
    if (clientDetails.length === 3) {
        const tenantId = clientDetails[0];
        const tenantName = clientDetails[1];
        const username = clientDetails[2];

        const persisted = await DDBUtils.addEntryWithTenantId(subscriptionID, tenantId, tenantName);

        const message = generateEventDetailsMessage({
            'ID': eventData.id,
            'Subscription ID': subscriptionID,
            'Tenant Name': tenantName,
            'Tenant ID': tenantId,
            'Persisted': persisted,
            'User': username,
        });
        if (persisted) {
            console.log("Checkout completed event processed successfully", subscriptionID, tenantId, tenantName, username);
            await SlackUtils.sendSuccessNotification('Checkout Completed - Processed Successfully', message);

        } else {
            console.log("Checkout completed event processing failed", subscriptionID, tenantId, tenantName, username);
            await SlackUtils.sendErrorNotification('Checkout Completed - Failed Processing', message);
        }
    } else {
        console.log("Checkout completed event processing failed. Incorrect client reference", subscriptionID, clientReference);
        await SlackUtils.sendErrorNotification('Checkout Completed - Failed Processing. Incorrect client reference', message, clientReference);
    }
}

async function handleSubscriptionCreatedEvent(eventData) {
    const message = generateEventDetailsMessage({
        'ID': eventData.id,
        'Customer': eventData.customer,
        'Item': eventData.plan.product,
        'Plan': `${eventData.plan.nickname} (${eventData.plan.id})`,
        'Package Name': eventData.plan.metadata['package_name'],
        'Package Category': eventData.plan.metadata['package_category'],
        'Ends On': moment.unix(eventData.current_period_end).format('YYYY-MMM-DD @ HH:mm'),
        'Status': eventData.status
    });
    await SlackUtils.sendSuccessNotification('Subscription Created', message);
}

async function handleSubscriptionUpdatedEvent(eventData) {
    const subscriptionID = eventData.id;
    const expiry = eventData.current_period_end;
    const isCancelled = eventData.cancel_at_period_end;
    const billingInterval = getBillingInterval(eventData.plan.interval);
    const packageName = eventData.plan.metadata['package_name'];
    const packageCategory = eventData.plan.metadata['package_category'];

    const status = isCancelled ? 'cancelled' : eventData.status;
    const persisted = await DDBUtils.addEntryWithSubscriptionDetails(subscriptionID, status, expiry, packageCategory, packageName, billingInterval);

    const message = generateEventDetailsMessage({
        'ID': subscriptionID,
        'Status': status,
        'Expiry': expiry,
        'Persisted': persisted,
        'Plan': `${eventData.plan.nickname} (${eventData.plan.id})`,
        'Package Name': packageName,
        'Package Category': packageCategory
    });

    if (persisted) {
        console.log("Subscription Updated event processed successfully", subscriptionID, status, expiry);
        await SlackUtils.sendSuccessNotification('Subscription Updated - Processed Successfully', message);

    } else {
        console.log("Subscription Updated event processing failed", subscriptionID, status, expiry);
        await SlackUtils.sendErrorNotification('Subscription Updated - Failed Processing', message);
    }

}

async function handleSubscriptionCancelledEvent(eventData) {
    const subscriptionID = eventData.id;

    const persisted = await DDBUtils.addEntryWithCancellation(subscriptionID);

    const message = generateEventDetailsMessage({
        'ID': subscriptionID,
        'Plan': eventData.plan.nickname,
        'Persisted': persisted,
        'Package Name': eventData.plan.metadata['package_name'],
        'Package Category': eventData.plan.metadata['package_category'],
    });
    if (persisted) {
        console.log("Subscription Cancelled event processed successfully", subscriptionID);
        await SlackUtils.sendSuccessNotification('Subscription Cancelled - Processed Successfully', message);

    } else {
        console.log("Subscription Cancelled event processing failed", subscriptionID);
        await SlackUtils.sendErrorNotification('Subscription Cancelled - Failed Processing', message);

    }

}

function getChargeDetails(eventData) {
    return generateEventDetailsMessage({
        'ID': eventData.id,
        'Amount': (eventData.amount / 100) + eventData.currency,
        'Name': eventData.billing_details.name,
        'Email': eventData.billing_details.email
    });
}

function getCustomerDetails(eventData) {
    return generateEventDetailsMessage({
        'CustomerID': eventData.id,
        'BillingEmail': eventData.email,
        'OrgName': eventData.name,
        'OrgAddress': JSON.stringify(eventData.address)
    });
}

function generateEventDetailsMessage(details = {}) {
    let message = '';
    Object.keys(details).forEach(key => {
        message += `${key}: ${SlackUtils.getTickedText(details[key])}\n`;
    });
    return message;
}

function getBillingInterval(interval) {
    switch (interval) {
        case 'month':
            return "MONTHLY";
        case 'year':
            return "ANNUALLY";
        default:
            return "CUSTOM"
    }
}

function getSuccessResponse() {
    return {
        "statusCode": 200
    };
};
