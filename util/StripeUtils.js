const SlackUtils = require('./SlackUtils');
const {STRIPE_PAYMENT_PLANS, STRIPE_PRODUCT_ID} = require('./Constants');

const ENDPOINT_SECRET = process.env.STRIPE_ENDPOINT_SECRET;
const API_SECRET = process.env.STRIPE_API_SECRET;

const stripe = require('stripe')(API_SECRET);

module.exports = {
    validateAndGetStripeEvent: async (lambdaEvent) => {
        let signature = lambdaEvent.headers['stripe-signature'] || lambdaEvent.headers['Stripe-Signature'];

        try {
            let stripeEvent = stripe.webhooks.constructEvent(lambdaEvent.body, signature, ENDPOINT_SECRET);
            console.log("Stripe event validated.");
            return stripeEvent;

        } catch (err) {
            console.log("Stripe event validation failed", err, lambdaEvent);
            await SlackUtils.sendErrorNotification(
                "Stripe event validation Failed",
                "Validation failed for the received Stripe event", err, JSON.stringify(lambdaEvent));
        }
    },

    getEventType: (event) => event['type'],
    getEventData: (event) => event['data']['object'],

    getSubscriptionDetails: async (subscriptionID, tenantId) => {
        console.log("Retrieving details of subscription. subscription id: %s, tenant id: %s", subscriptionID, tenantId);
        return await stripe.subscriptions.retrieve(subscriptionID);
    },

    cancelSubscriptionAtPeriodEnd: async (subscriptionID, tenantId) => {
        console.log("Cancelling subscription", subscriptionID, "at period end. tenant id: %s", tenantId);
        return await stripe.subscriptions.update(subscriptionID, {cancel_at_period_end: true});
    },

    abortSubscriptionCancellation: async (subscriptionID, tenantId) => {
        console.log("Aborting Cancellation of subscription. subscription id: %s, tenant id: %s", subscriptionID, tenantId);
        return await stripe.subscriptions.update(subscriptionID, {cancel_at_period_end: false});
    },
    updateSubscription: async (subscriptionID, subscriptionItemId, oldPlanId, newPlanId, tenantId) => {
        console.log("Updating subscription: %s, old plan: %s, to new Subscription plan: %s, new plan: %s, tenant id: %s", subscriptionID, subscriptionItemId, oldPlanId, newPlanId, tenantId);
        return await stripe.subscriptions.update(subscriptionID, {
            items: [
                {plan: oldPlanId, quantity: 1, id: subscriptionItemId, deleted: true},
                {plan: newPlanId, quantity: 1}
            ],
            proration_behavior: 'always_invoice'
        });
    },

    addStripePackages: async () => {
        for (const stripePackage of STRIPE_PAYMENT_PLANS) {
            await stripe.plans.create(
                {
                    amount: stripePackage.amount,
                    currency: 'usd',
                    interval: stripePackage.interval,
                    product: STRIPE_PRODUCT_ID,
                    nickname: stripePackage.nickname,
                    metadata: {
                        package_name: stripePackage.name,
                        package_category: stripePackage.category
                    }
                }
            )
        }
        return true;
    },
    createCustomer: async (tenantEmail, organizationName, address, tenantId, tenantName) => {
        return await stripe.customers.create(
            {
                email: tenantEmail,
                name: organizationName,
                address: address,
                metadata: {
                    tenant_id: tenantId,
                    tenant_name: tenantName
                }
            }
        );
    },
    createSession: async (customerId, tenantId, tenantName, userEmail, planId, successUrl, cancelUrl) => {
        return await stripe.checkout.sessions.create(
            {
                success_url: successUrl,
                cancel_url: cancelUrl,
                payment_method_types: ['card'],
                line_items: [
                    {
                        price: planId,
                        quantity: 1,
                    },
                ],
                mode: 'subscription',
                client_reference_id: `${tenantId}#${tenantName}#${userEmail}`,
                customer: customerId
            }
        );
    }
};
