const AWS = require('aws-sdk');
const ddb = new AWS.DynamoDB.DocumentClient();
const SlackUtils = require('./SlackUtils');

module.exports = {
    addEntryWithTenantId: async (subscriptionID, tenantId, tenantName) => {
        try {
            await ddb.update({
                TableName: "StripeSubscriptions",
                Key: {
                    sub_id: subscriptionID
                },
                ExpressionAttributeNames: {
                    '#tenantIdField': "tenant_id",
                    '#tenantNameField': "tenant_name"
                },
                ExpressionAttributeValues: {
                    ':tenantIdValue': +tenantId,
                    ':tenantNameValue': tenantName
                },
                UpdateExpression: "set #tenantIdField = :tenantIdValue, #tenantNameField = :tenantNameValue"
            }).promise();
            console.log("Successfully added entry with subscription ID, tenantID and tenant name", subscriptionID, tenantId, tenantName);
            return true;

        } catch (error) {
            console.log("Failed to add an entry with subscription ID, tenant ID and tenant name", subscriptionID, tenantId, tenantName, error);
            await SlackUtils.sendErrorNotification(
                "Stripe SubscriptionDDB Persistance Failed",
                `Failed to add an entry with subscription ID: ${subscriptionID}, tenant ID: ${tenantId} and tenantName: ${tenantName}`, error);
            return false;
        }
    },

    addEntryWithSubscriptionDetails: async (subscriptionID, status, expiry, packageCategory, packageName, billingInterval) => {
        try {
            await ddb.update({
                TableName: "StripeSubscriptions",
                Key: {
                    sub_id: subscriptionID
                },
                ExpressionAttributeNames: {
                    '#statusField': "status",
                    '#expiryField': "expiry",
                    '#packageCategoryField': "package_category",
                    '#packageNameField': "package_name",
                    '#billingIntervalField': "billing_interval",
                },
                ExpressionAttributeValues: {
                    ':statusValue': status,
                    ':expiryValue': expiry,
                    ':packageCategoryValue': packageCategory,
                    ':packageNameValue': packageName,
                    ':billingIntervalValue': billingInterval
                },
                UpdateExpression: "set #statusField = :statusValue , #expiryField = :expiryValue, #packageCategoryField = :packageCategoryValue, #packageNameField = :packageNameValue, #billingIntervalField = :billingIntervalValue"
            }).promise();
            console.log("Successfully added entry with subscription ID, status and expiry", subscriptionID, status, expiry);
            return true;

        } catch (error) {
            console.log("Failed to add an entry with subscription ID, status and expiry", subscriptionID, status, expiry, error, packageCategory, packageName);
            await SlackUtils.sendErrorNotification(
                "Stripe SubscriptionDDB Persistance Failed",
                `Failed to add an entry with subscription ID: ${subscriptionID}, status: ${status}, package category: ${packageCategory}, package name: ${packageName} and expiry: ${expiry}`, error);
            return false;
        }
    },

    addEntryWithCancellation: async (subscriptionID) => {
        try {
            await ddb.update({
                TableName: "StripeSubscriptions",
                Key: {
                    sub_id: subscriptionID
                },
                ExpressionAttributeNames: {
                    '#statusField': "status"
                },
                ExpressionAttributeValues: {
                    ':statusValue': "cancelled"
                },
                UpdateExpression: "set #statusField = :statusValue"
            }).promise();
            console.log("Successfully updated the status of subscription to cancelled", subscriptionID);
            return true;

        } catch (error) {
            console.log("Failed to update the status of subscription to cancelled", subscriptionID, error);
            await SlackUtils.sendErrorNotification(
                "Stripe Subscription DDB Persistance Failed",
                `Failed to update the status of subscription: ${subscriptionID} to cancelled`, error);
            return false;
        }
    },
    getSubscriptionByTenantId: async (tenantId) => {
        try {
            const data = await ddb.query({
                TableName: "StripeSubscriptions",
                IndexName: "GSI-1",
                KeyConditionExpression:
                    '#tenantIdField = :tenantIdValue',
                ExpressionAttributeNames: {
                    '#tenantIdField': "tenant_id"
                },
                ExpressionAttributeValues: {
                    ':tenantIdValue': +tenantId
                }
            }).promise();
            console.log("Successfully retrieved subscription details by tenant id:", tenantId);
            return data;

        } catch (error) {
            console.log("Failed to retrieved subscription details  by tenant id", tenantId, error);
        }
    },
    updateTenantBillingDetails: async (tenantId, orgName, billingAddress, billingEmail) => {
        try {
            await ddb.update({
                TableName: "Tenant",
                Key: {
                    tenant_id: Number(tenantId),
                    tenant_type: 0,
                },
                ExpressionAttributeNames: {
                    '#orgName': "organization_name",
                    '#billingEmail': "billing_email",
                    '#billingAddress': "billing_address"
                },
                ExpressionAttributeValues: {
                    ':orgName': orgName,
                    ':billingEmail': billingEmail,
                    ':billingAddress': billingAddress,
                },
                UpdateExpression: "set #orgName = :orgName, #billingEmail = :billingEmail, #billingAddress = :billingAddress"
            }).promise();
            console.log("Successfully updated billing details with tenant id: %s, organization name: %s", tenantId, orgName);
            return true;

        } catch (error) {
            console.log("Failed to update billing details with tenant id: %s, organization name: %s", tenantId, orgName, error);
            return false;
        }
    },
}
