const {IncomingWebhook} = require('@slack/webhook');
const {ALERT_SLACK_CHANNEL} = require('../util/Constants');

const url = process.env.SLACK_WEBHOOK_URL;
const webhook = new IncomingWebhook(url);

module.exports = {
    sendSuccessNotification: async (header, message, icon = 'bulb') => {
        await webhook.send({
            "attachments": [
                {
                    "fallback": `${header}`,
                    "color": "#2eb886",
                    "title": `:${icon}: ${header}`,
                    "text": message,
                    "ts": Date.now()
                }
            ],
            "channel": `#${ALERT_SLACK_CHANNEL}`
        });
    },
    sendErrorNotification: async (header, message, ...errorDetails) => {

        let errDetailsBlocks = (errorDetails && errorDetails.length > 0) ? '\n' + errorDetails.map(d => '```' + d + '```').join('\n') : '';

        await webhook.send({
            "attachments": [
                {
                    "fallback": `${header}`,
                    "color": "#f20a2d",
                    "title": `:warning: ${header}`,
                    "text": message + errDetailsBlocks,
                    "ts": Date.now()
                }
            ],
            "channel": `#${ALERT_SLACK_CHANNEL}`
        });
    },
    getTickedText: (text) => `\`${text}\``
}
