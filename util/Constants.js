module.exports = {
    USER_ATTRIBUTES: {
        TENANT_ID: "given_name",
        TENANT_NAME: "name",
        COGNITO_USER_NAME: "cognito:username",
    },
    STRIPE_PAYMENT_PLANS: [
        {category: "STARTER", name: "PERSONAL", nickname: "PERSONAL_MONTHLY", interval: 'month', amount: 900},
        {category: "STARTER", name: "LITE", nickname: "LITE_MONTHLY", interval: 'month', amount: 2900},
        {category: "STARTER", name: "BASIC", nickname: "BASIC_MONTHLY", interval: 'month', amount: 4900},
        {category: "STARTER", name: "STARTUP", nickname: "STARTUP_MONTHLY", interval: 'month', amount: 9900},
        {category: "BUSINESS", name: "SMALL", nickname: "SMALL_MONTHLY", interval: 'month', amount: 24900},
        {category: "BUSINESS", name: "BUSINESS", nickname: "BUSINESS_MONTHLY", interval: 'month', amount: 39900},
        {category: "BUSINESS", name: "PREMIUM", nickname: "PREMIUM_MONTHLY", interval: 'month', amount: 59900},
        {category: "BUSINESS", name: "ADVANCED", nickname: "ADVANCED_MONTHLY", interval: 'month', amount: 79900},
        {category: "ENTERPRISE", name: "BRONZE", nickname: "BRONZE_MONTHLY", interval: 'month', amount: 99900},
        {category: "ENTERPRISE", name: "SILVER", nickname: "SILVER_MONTHLY", interval: 'month', amount: 199900},
        {category: "ENTERPRISE", name: "GOLD", nickname: "GOLD_MONTHLY", interval: 'month', amount: 299900},
        {category: "ENTERPRISE", name: "PLATINUM", nickname: "PLATINUM_MONTHLY", interval: 'month', amount: 399900}
    ],
    ALERT_SLACK_CHANNEL: 'mftg-stripe-staging'
};
