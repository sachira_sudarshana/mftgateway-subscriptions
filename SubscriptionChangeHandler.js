const moment = require('moment');
const SlackUtils = require('./util/SlackUtils');

exports.handler = async (event) => {

    try {
        const records = event.Records;
        console.log("Received", records.length, "record(s) for processing");

        const refinedRecords =
            records.filter(r => r['eventName'] !== 'REMOVE')    // Ignore remove events
                .map(r => r['dynamodb']['NewImage']).filter(r => {
                let {subscriptionID, tenantId, status} = getRecordFields(r);
                return (subscriptionID && tenantId && status && (status === 'active'));
            });

        console.log("Selected", refinedRecords.length, "record(s) for processing");
        for (let i = 0; i < refinedRecords.length; i++) {
            let r = refinedRecords[i];
            let {subscriptionID, tenantId, tenantName, expiry, packageCategory, packageName, billingInterval} = getRecordFields(r);
            console.log("Subscription successfully updated tenant with tenant ID", tenantId, ",tenant name", tenantName, "subscription ID", subscriptionID, "Package Category:", packageCategory, "Package Name:", packageName, "Billing Interval:", billingInterval, "expiry on", expiry);
            await SlackUtils.sendSuccessNotification('User Upgraded Successfully',
                `Subscription successfully updated tenant with tenant ID: \`${tenantId}\` ,tenant name: \`${tenantName}\` , (Subscription ID: \`${subscriptionID}\`, , Package Category: \`${packageCategory}\`, Package Name: \`${packageName}\`, Billing Interval: \`${billingInterval}\`, expires on: \`${moment.unix(expiry).format('YYYY-MMM-DD @ HH:mm')}\`)`);
        }
    } catch (err) {
        console.log("Subscription change processing failed", err, JSON.stringify(event));
        await SlackUtils.sendErrorNotification('Subscription change processing failed',
            'Failed to process subscription change event', err, JSON.stringify(event));
    }

    return true;
};

function getRecordFields(record) {
    return {
        subscriptionID: record['sub_id']['S'],
        tenantId: record['tenant_id'] ? record['tenant_id']['N'] : undefined,
        tenantName: record['tenant_name'] ? record['tenant_name']['S'] : undefined,
        status: record['status'] ? record['status']['S'] : undefined,
        expiry: record['expiry'] ? Number(record['expiry']['N']) : undefined,
        packageCategory: record['package_category'] ? record['package_category']['S'] : undefined,
        packageName: record['package_name'] ? record['package_name']['S'] : undefined,
        billingInterval: record['billing_interval'] ? record['billing_interval']['S'] : undefined
    };
}
